<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');

class API{
    private $db;
    public function __construct()
    {
        $this->db = new MysqliDb('localhost','root','','employee');
    }
    public function httpGet($payload = array()){
        $query = $this->db->get('information');
        if($query){
            return json_encode(array(
                'method'=>'GET',
                'status'=>'success',
                'data'=>$query,
            ));
        }else{
            return json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'data'=>[],
                'message'=>'Failed to Fetch'
            ));
        }
    }
    public function httpPost($payload){

        // $query = $this->db->insert('information',$payload);

        // if($query){
        //     return json_encode(array(
        //         'method'=>'POST',
        //         'status'=>'success',
        //         'data'=>$payload,
        //     ));
        // }else{
        //     return json_encode(array(
        //         'method'=>'POST',
        //         'status'=>'fail',
        //         'data'=>[],
        //         'message'=>'Failed to Insert'
        //     ));
        // }

        if(empty($payload)){
            return json_encode(array(
                'method'=>'POST',
                'status'=>'fail',
                'data'=>[],
                'message'=>'No data provided'
            ));
            return;
        }
    
        $required_fields = array('first_name', 'middle_name', 'last_name','contact_number');
        foreach($required_fields as $field){
            if(!array_key_exists($field, $payload)){
                return json_encode(array(
                    'method'=>'POST',
                    'status'=>'fail',
                    'data'=>[],
                    'message'=>"Missing required field: $field"
                ));
                return;
            }
        }
    
        $query = $this->db->insert('information',$payload);
        if($query){
            return json_encode(array(
                'method'=>'POST',
                'status'=>'success',
                'data'=>$payload,
            ));
        }else{
            return json_encode(array(
                'method'=>'POST',
                'status'=>'fail',
                'data'=>[],
                'message'=>'Failed to Insert'
            ));
        }
    }
    public function httpPut($id, $payload){
        if(empty($payload) || !is_array($payload)){
            return json_encode(array(
                'method'=>'PUT',
                'status'=>'fail',
                'data'=>[],
                'message'=>'Invalid Payload'
            ));
            return;
        }
    
        $this->db->where('id', $id);
        $query = $this->db->update('information', $payload);
        if($query){
            return json_encode(array(
                'method'=>'PUT',
                'status'=>'success',
                'data'=>$payload,
            ));
        }else{
            return json_encode(array(
                'method'=>'PUT',
                'status'=>'fail',
                'data'=>[],
                'message'=>'Failed to Update'
            ));
        }
    }
    public function httpDelete($id, $payload)
    {
        $selected_id = ['id' => []];
        if (is_string($id)) {
            $selected_id['id'] = explode(",", $id);
        } elseif (!is_null($id)) {
            $selected_id['id'] = [$id];
        }

        if (count($selected_id['id'])) {
            $this->db->where('id', $selected_id['id'], 'IN');
        } else {
            $this->db->where('id', $id);
        }

        $query = $this->db->delete('information');

        if ($query) {
            return json_encode([
                'method' => 'DELETE',
                'status' => 'success',
                'data' => $payload,
            ]);
        } else {
            return json_encode([
                'method' => 'DELETE',
                'status' => 'fail',
                'data' => [],
                'message' => 'Failed to Delete',
            ]);
        }
    }
    // public function httpDelete($id,$payload){
    //     $selected_id = ['id'=>is_string($id)?explode(",",$id):null];
    //     if(count($selected_id['id'])){
    //         $this->db->where('id',$selected_id['id'],'IN');
    //     }
    //     else{
    //         $this->db->where('id',$id);
    //     }
    //     $query = $this->db->delete('information');
    //     if($query){
    //         return json_encode(array(
    //             'method'=>'DELETE',
    //             'status'=>'success',
    //             'data'=>$payload,
    //         ));
    //         return;
    //     }else{
    //         return json_encode(array(
    //             'method'=>'DELETE',
    //             'status'=>'fail',
    //             'data'=>[],
    //             'message'=>'Failed to Delete'
    //         ));
    //     }
    // }
}

// if (isset($_SERVER['REQUEST_METHOD'])) {
//     $request_method = $_SERVER['REQUEST_METHOD'];
//  }
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method ==='GET'){
    $received_data = $_GET;
}else{
    if($request_method==='PUT'||$request_method==='DELETE'){
        $request_uri = $_SERVER['REQUEST_URI'];
        $ids = null;
        $exploded_request_uri = array_values(explode("/",$request_uri));
        $last_index = count($exploded_request_uri)-1;
        $ids = $exploded_request_uri[$last_index];
    }
    $received_data = json_decode(file_get_contents('php://input'),true);
}

$api = new API;

switch($request_method){
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids,$received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids,$received_data);
        break;
}
?>