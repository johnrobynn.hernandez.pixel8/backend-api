<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APITest extends TestCase{
    private $api;
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpPost(){
        $_SERVER['REQUEST_METHOD']='POST';
        $payload = array(
            'first_name'=>'Test',
            'middle_name'=>'test',
            'last_name'=>'last test',
            'contact_number'=>654655
        );
        $result = json_decode($this->api->httpPost($payload),true);
        $this->assertArrayHasKey('status',$result);
        $this->assertEquals($result['status'],'success');
        $this->assertArrayHasKey('data',$result);
    }
    public function testHttpPut(){
        $_SERVER['REQUEST_METHOD']='PUT';
        $payload = array(
            'first_name'=>'Updated',
            'middle_name'=>'Test',
            'last_name'=>'User',
            'contact_number'=>23456
        );
        $result = json_decode($this->api->httpPut(1,$payload),true);
        $this->assertArrayHasKey('status',$result);
        $this->assertEquals($result['status'],'success');
        $this->assertArrayHasKey('data',$result);
    }
    
    public function testHttpGet(){
        $_SERVER['REQUEST_METHOD']='GET';
        $result = json_decode($this->api->httpGet(),true);
        $this->assertArrayHasKey('status',$result);
        $this->assertEquals($result['status'],'success');
        $this->assertArrayHasKey('data',$result);
    }
    
    public function testHttpDelete(){
        $_SERVER['REQUEST_METHOD']='DELETE';
        // $id=array(
        //     'id'=>'1'
        // );
        $payload = array(
            'first_name'=>'Test',
            'middle_name'=>'test',
            'last_name'=>'last test',
            'contact_number'=>654655
        );
        $result = json_decode($this->api->httpDelete(1,$payload),true);
        $this->assertArrayHasKey('status',$result);
        $this->assertEquals($result['status'],'success');
        $this->assertArrayHasKey('data',$result);
    }
}
?>